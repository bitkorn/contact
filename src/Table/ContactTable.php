<?php

namespace Bitkorn\Contact\Table;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ContactTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'contact';

    /**
     * @param string $contactUuid
     * @return array
     */
    public function getContact(string $contactUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['contact_uuid' => $contactUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertContact(ContactEntity $contactEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $contactEntity->setPrimaryKeyValue($uuid);
        try {
            $insert->values($contactEntity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $contactUuid
     * @return int
     */
    public function deleteContact(string $contactUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['contact_uuid' => $contactUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $contactUuid
     * @param ContactEntity $contactEntity
     * @return int
     */
    public function updateContact(string $contactUuid, ContactEntity $contactEntity): int
    {
        $update = $this->sql->update();
        $contactEntity->unsetPrimaryKey();
        try {
            $update->set($contactEntity->getStorage());
            $update->where(['contact_uuid' => $contactUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param Select $selectContactUuids A Select that only gives me contact_uuid's back.
     * @return array
     */
    public function getContacts(Select $selectContactUuids)
    {
        $select = $this->sql->select();
        try {
            $select->where->in('contact_uuid', $selectContactUuids);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
