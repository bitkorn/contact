<?php

namespace Bitkorn\Contact\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;

abstract class AbstractContactTable extends AbstractLibTable
{
    protected array $contactColumns = ['contact_label', 'contact_salut', 'contact_name', 'contact_dept', 'contact_tel', 'contact_fax', 'contact_mobile', 'contact_email', 'contact_www'];

    protected ContactTable $contactTable;

    public function setContactTable(ContactTable $contactTable): void
    {
        $this->contactTable = $contactTable;
    }

}
