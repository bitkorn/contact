<?php

namespace Bitkorn\Contact\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ContactEntity extends AbstractEntity
{
    public array $mapping = [
        'contact_uuid' => 'contact_uuid',
        'contact_label' => 'contact_label',
        'contact_salut' => 'contact_salut',
        'contact_name' => 'contact_name',
        'contact_department' => 'contact_department',
        'contact_tel' => 'contact_tel',
        'contact_fax' => 'contact_fax',
        'contact_mobile' => 'contact_mobile',
        'contact_email' => 'contact_email',
        'contact_www' => 'contact_www',
        'contact_desc' => 'contact_desc',
    ];

    protected $primaryKey = 'contact_uuid';

    public function getContactUuid(): string
    {
        if (!isset($this->storage['contact_uuid'])) {
            return '';
        }
        return $this->storage['contact_uuid'];
    }

    public function getContactLabel(): string
    {
        if (!isset($this->storage['contact_label'])) {
            return '';
        }
        return $this->storage['contact_label'];
    }

    public function getContactSalut(): string
    {
        if (!isset($this->storage['contact_salut'])) {
            return '';
        }
        return $this->storage['contact_salut'];
    }

    public function getContactName(): string
    {
        if (!isset($this->storage['contact_name'])) {
            return '';
        }
        return $this->storage['contact_name'];
    }

    public function getContactDepartment(): string
    {
        if (!isset($this->storage['contact_department'])) {
            return '';
        }
        return $this->storage['contact_department'];
    }

    public function getContactTel(): string
    {
        if (!isset($this->storage['contact_tel'])) {
            return '';
        }
        return $this->storage['contact_tel'];
    }

    public function getContactFax(): string
    {
        if (!isset($this->storage['contact_fax'])) {
            return '';
        }
        return $this->storage['contact_fax'];
    }

    public function getContactMobile(): string
    {
        if (!isset($this->storage['contact_mobile'])) {
            return '';
        }
        return $this->storage['contact_mobile'];
    }

    public function getContactEmail(): string
    {
        if (!isset($this->storage['contact_email'])) {
            return '';
        }
        return $this->storage['contact_email'];
    }

    public function getContactWww(): string
    {
        if (!isset($this->storage['contact_www'])) {
            return '';
        }
        return $this->storage['contact_www'];
    }

    public function getContactDesc(): string
    {
        if (!isset($this->storage['contact_desc'])) {
            return '';
        }
        return $this->storage['contact_desc'];
    }
}
