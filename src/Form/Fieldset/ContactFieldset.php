<?php

namespace Bitkorn\Contact\Form\Fieldset;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\Form\AbstractFieldset;
use Bitkorn\Trinket\Hydrator\EntityHydrator;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\Digits;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\InputFilter\InputProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\Hostname;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ContactFieldset extends AbstractFieldset implements InputFilterProviderInterface
{

    public function init()
    {
        $this->setName('contact_fieldset');
        $this->setHydrator(new EntityHydrator());
        $this->setObject(new ContactEntity());

        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'contact_uuid']);
        }

        $this->add(['name' => 'contact_label']);
        $this->add(['name' => 'contact_salut']);
        $this->add(['name' => 'contact_name']);
        $this->add(['name' => 'contact_dept']);
        $this->add(['name' => 'contact_tel']);
        $this->add(['name' => 'contact_fax']);
        $this->add(['name' => 'contact_mobile']);
        $this->add(['name' => 'contact_email']);
        $this->add(['name' => 'contact_www']);
    }

    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['contact_uuid'] = ['required' => true, 'validators' => [['name' => Uuid::class]]];
        }

        $filter['contact_label'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['contact_salut'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['contact_name'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['contact_dept'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['contact_tel'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['contact_fax'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['contact_mobile'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['contact_email'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => EmailAddress::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['contact_www'] = [
            'required' => false,
            'filters' => [['name' => StringTrim::class], ['name' => HtmlEntities::class], ['name' => StripTags::class]],
            'validators' => [
                [
                    'name' => Hostname::class,
                ]
            ]
        ];

        return $filter;
    }
}
