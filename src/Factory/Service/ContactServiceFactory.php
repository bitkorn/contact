<?php

namespace Bitkorn\Contact\Factory\Service;

use Bitkorn\Contact\Service\ContactService;
use Bitkorn\Contact\Table\ContactTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ContactServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ContactService();
        $service->setLogger($container->get('logger'));
        $service->setContactTable($container->get(ContactTable::class));
        return $service;
    }
}
