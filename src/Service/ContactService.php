<?php

namespace Bitkorn\Contact\Service;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Contact\Table\ContactTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class ContactService extends AbstractService
{
    /**
     * @var ContactTable
     */
    protected $contactTable;

    /**
     * @param ContactTable $contactTable
     */
    public function setContactTable(ContactTable $contactTable): void
    {
        $this->contactTable = $contactTable;
    }

    public function insertContact(ContactEntity $contactEntity): string
    {
        return $this->contactTable->insertContact($contactEntity);
    }

    public function deleteContact(string $contactUuid): bool
    {
        return $this->contactTable->deleteContact($contactUuid) > 0;
    }

    public function updateContact(string $contactUuid, ContactEntity $contactEntity): bool
    {
        return $this->contactTable->updateContact($contactUuid, $contactEntity) >= 0;
    }
}
