<?php

namespace Bitkorn\Contact;

use Bitkorn\Contact\Factory\Form\Fieldset\ContactFieldsetFactory;
use Bitkorn\Contact\Form\Fieldset\ContactFieldset;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            Service\ContactService::class => Factory\Service\ContactServiceFactory::class,
            // table
            Table\ContactTable::class => Factory\Table\ContactTableFactory::class,
            // fieldset
            ContactFieldset::class => ContactFieldsetFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
