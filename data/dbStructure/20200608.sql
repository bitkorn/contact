create table public.contact
(
    contact_uuid   uuid not null
        constraint contact_pk
            primary key,
    contact_salut  varchar(20),
    contact_name   varchar(200),
    contact_dept   varchar(200),
    contact_tel    varchar(100),
    contact_fax    varchar(100),
    contact_mobile varchar(100),
    contact_email  varchar(200),
    contact_www    varchar(200)
);

comment on column public.contact.contact_salut is 'salutation / Anrede';

comment on column public.contact.contact_dept is 'department';

alter table public.contact
    owner to postgres;

